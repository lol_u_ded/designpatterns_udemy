package a4.übung;

class Person
{
    public int id;
    public String name;

    public Person(int id, String name)
    {
        this.id = id;
        this.name = name;
    }
}

class PersonFactory
{
    private static int i = -1;
    public Person createPerson(String name)
    {
        i++;
        return new Person(i, name);
    }
}